import os
import requests
from bs4 import BeautifulSoup as bs
import webbrowser as wb

query = input('enter the song name: ')
query = query.replace(' ', '+')
url = 'https://www.youtube.com/results?search_query=' + query
source_code = requests.get(url,timeout=15)
text = source_code.text

soup = bs(text, 'html.parser')
songs = soup.findAll('div', {'class': 'yt-lockup-video'})
song = songs[0].contents[0].contents[0].contents[0]
link = song['href']
wb.open('https://www.youtube.com' + link)
