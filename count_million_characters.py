#!/usr/bin/python3
import sys
import pprint

#pp = pprint.PrettyPrinter(indent=4)

def count_chars(file_name):
    count = {}
    with open(file_name) as info:
        readfile = info.read()
        for character in readfile.upper():
            count[character] = count.get(character, 0) + 1

    return count

if __name__ == '__main__':
    if sys.version_info.major >= 3:
        input_func = input
    else:
        input_func = raw_input

    input_file = input_func("File Name: ")
    pprint.pprint(count_chars(input_file))
